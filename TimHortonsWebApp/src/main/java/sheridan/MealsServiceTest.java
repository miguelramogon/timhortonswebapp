package sheridan;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

public class MealsServiceTest {

	@Test
	public void testDrinksRegular() {
		MealsService test = new MealsService();
		List<String> listString = test.getAvailableMealTypes(MealType.DRINKS);
		assertFalse(listString.isEmpty());
	}
	
	@Test
	public void testDrinksException() {
		MealsService test = new MealsService();
		List<String> listString = test.getAvailableMealTypes(null);
		assertTrue(listString.get(0).equals("No Brand Available"));
	}
	
	@Test
	public void testDrinksBoundaryIn() {
		MealsService test = new MealsService();
		List<String> listString = test.getAvailableMealTypes(MealType.DRINKS);
		assertTrue(listString.size() > 3);
	}
	
	@Test
	public void testDrinksBoundaryOut() {
		MealsService test = new MealsService();
		List<String> listString = test.getAvailableMealTypes(null);
		assertFalse(listString.size() > 1);
	}

}
